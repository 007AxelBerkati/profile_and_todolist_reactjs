import React from 'react';
import Axel from '../../assets/image/Axel.png';

function Profile() {
  return (
    <div className="container">
      <div className="title_wrapper">
        <div className="title satu">
          <div className="image-wrapper">
            <img src={Axel} width="175" height="175" />
          </div>
        </div>
        <div className="title dua">
          <h1>Axel Berkati</h1>
          <p>Mahasiswa</p>
          <div className="button-wrapper">
            <button className="btn download">Download Cv</button>
            <button className="btn hire">Hire Me</button>
          </div>
        </div>
      </div>

      <div className="body_wrapper">
        <div className="about_me">
          <div className="description satu">
            <h1>About Me</h1>
            <p>
              Halo, Nama saya Axel Berkati, Saya berkuliah di
              Universitas Palangka Raya, jurusan Teknik Informatika.
              Saya Sekarang sedang mengikuti Studi Independen di Binar
              Academy, dan mengambil kelas React Native.
            </p>
          </div>
          <div className="description dua">
            <p>
              <b>Age</b>
            </p>
            <br />
            <p>
              <b>Email</b>
            </p>
            <br />
            <p>
              <b>Phone</b>
            </p>
            <br />
            <p>
              <b>Adress</b>
            </p>
          </div>
          <div className="description tiga">
            <p>20</p>
            <br />
            <p>bucel1916@gmail.com</p>
            <br />
            <p>081250386069</p>
            <br />
            <p>Palangka Raya, Kalimantan Tengah</p>
          </div>
        </div>
        <div className="contact_me">
          <div className="contact satu">
            <h1>Contact</h1>
            <div className="identity">
              <input
                type="text"
                placeholder="Your Name"
                className="identity-input"
              />
              <input
                type="text"
                placeholder="Your Email"
                className="identity-input"
              />
            </div>
            <div className="message">
              <input
                type="text"
                placeholder="Your Message"
                className="identity-message"
              />
            </div>
            <button className="btn submit">Submit</button>
          </div>
          <div className="contact dua">
            <p>
              <b>Adress</b>
            </p>
            <br />
            <p>Palangka Raya, Kalimantan Tengah</p>
            <br />
            <p>
              <b>Phone</b>
            </p>
            <br />
            <p>081250386069</p>
            <br />
            <p>
              <b>Email</b>
            </p>
            <br />
            <p>bucel1916@gmail.com</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;
