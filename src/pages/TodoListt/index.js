import React from 'react';
import TodoList from '../../component/TodoList';

function TodoListt() {
  return (
    <div>
      <div className="todo-list-wrapper">
        <div className="todo-app">
          <TodoList />
        </div>
      </div>
    </div>
  );
}

export default TodoListt;
