import React, { useEffect, useState } from 'react';
import TodoForm from './TodoForm';
import Todo from './Todo';

function TodoList() {
  // inisiasi untuk nilai awal todos sama dengan array kosong. todos dipakai untuk menampung semua isi todo list
  const [todos, setTodos] = useState([]);

  // menjalankan block code dari arrow function hanya ketika component pertama kali dirender
  useEffect(() => {
    const json = localStorage.getItem('itemTodos');
    const loadedTodos = JSON.parse(json);
    if (loadedTodos) {
      setTodos(loadedTodos);
    }
  }, []);

  // menjalankan block code dari arrow function hanya ketika component pertama kali dirender, dan juga ketika value todos berubah
  useEffect(() => {
    const json = JSON.stringify(todos); // Mengubah array todos menjadi string JSON, karena local storage hanya bisa menerima dalam bentuk tipe string
    localStorage.setItem('itemTodos', json); // Menyimpan data array todos yang telah menjadi string JSON, ke ItemTodos
  }, [todos]);

  // Fungsi yang dijalankan ketika fungsi component TodoForm disubmit
  const addTodo = (todo) => {
    if (/^\s*$/.test(todo.text)) {
      return;
    }

    // menggabungkan data yang sebelumnya dengan data yang baru
    const newTodos = [...todos, todo];
    setTodos(newTodos);
  };

  const updateTodo = (todoId, newValue) => {
    // Jika value text tidak ada isinya selain spasi
    if (/^\s*$/.test(newValue.text)) {
      return;
    }

    // mengganti value todos menjadi value yang baru saja diganti, yang memiliki id yang sama
    setTodos((prev) =>
      prev.map((item) => (item.id === todoId ? newValue : item))
    );
  };

  // fungsi untuk filter array, yang tidak sama dengan id yang dipilih
  const removeTodo = (id) => {
    const removedArr = [...todos].filter((todo) => todo.id !== id);
    setTodos(removedArr);
  };

  const completeTodo = (id) => {
    let updatedTodos = [...todos].map((todo) => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });
    setTodos(updatedTodos);
  };

  return (
    <>
      <h1 className="todo h1">Todo List</h1>
      <TodoForm onSubmit={addTodo} />
      <Todo
        todos={todos}
        completeTodo={completeTodo}
        removeTodo={removeTodo}
        updateTodo={updateTodo}
      />
    </>
  );
}

export default TodoList;
