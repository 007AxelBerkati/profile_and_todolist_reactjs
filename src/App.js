import React from 'react';
import './App.css';
import './TodoList.css';

import { Profile, TodoListt } from './pages';

function App() {
  return (
    <div>
      <Profile />
      <TodoListt />
    </div>
  );
}

export default App;
